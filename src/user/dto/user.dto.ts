import { IsNotEmpty, IsString, Min } from 'class-validator';

export class UserDTO {
  @IsNotEmpty()
  @IsString()
  readonly names: string;
  @IsNotEmpty()
  @IsString()
  @Min(5)
  readonly username: string;
  @IsNotEmpty()
  @IsString()
  readonly email: string;
  @IsNotEmpty()
  @IsString()
  readonly password: string;
}
