export interface IUser extends Document {
  names: string;
  username: string;
  email: string;
  password: string;
}
